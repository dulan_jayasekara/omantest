package com.rezgateway.automation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import java.net.URL;

import com.rezgateway.automation.enu.RequestType;
import com.rezgateway.automation.pojo.HttpResponse;

public class HttpHandler {

	private String USER_AGENT = "Mozilla/5.0";

	// private String GET_URL = "http://localhost:9090";

	// private String POST_URL = "http://localhost:9090/home";

	public HttpResponse sendGET(String URL) throws IOException {

		HttpResponse httpresponse = new HttpResponse();
		HttpURLConnection con = getConnection(URL, RequestType.GET);

		int responseCode = con.getResponseCode();
		httpresponse.setRESPONSE_CODE(responseCode);

		System.out.println("GET Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			httpresponse.setRESPONSE(response.toString());
			return httpresponse;
		} else {
			System.out.println("GET request not worked");
			httpresponse.setRESPONSE("N/A");
			return httpresponse;

		}

	}

	public HttpResponse sendPOST(String URL, String Message) throws IOException {

		HttpResponse httpresponse = new HttpResponse();
		HttpURLConnection con = getConnection(URL, RequestType.POST);

		// For POST only - START
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
		os.write(Message.getBytes());
		os.flush();
		os.close();
		// For POST only - END

		int responseCode = con.getResponseCode();
		httpresponse.setRESPONSE_CODE(responseCode);

		System.out.println("POST Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			httpresponse.setRESPONSE(response.toString());
			return httpresponse;
		} else {

			System.out.println("POST request not worked");
			httpresponse.setRESPONSE("N/A");
			return httpresponse;
		}
	}

	public HttpResponse sendGET(HttpURLConnection con) throws IOException {

		HttpResponse httpresponse = new HttpResponse();
		int responseCode = con.getResponseCode();
		httpresponse.setRESPONSE_CODE(responseCode);

		System.out.println("GET Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			// print result
			httpresponse.setRESPONSE(response.toString());
			return httpresponse;
		} else {
			System.out.println("GET request not worked");
			httpresponse.setRESPONSE("N/A");
			return httpresponse;

		}

	}

	public HttpResponse sendPOST(HttpURLConnection con, String Message)
			throws IOException {
		HttpResponse httpresponse = new HttpResponse();
		// For POST only - START
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
		os.write(Message.getBytes());
		os.flush();
		os.close();
		// For POST only - END

		int responseCode = con.getResponseCode();
		httpresponse.setRESPONSE_CODE(responseCode);
		System.out.println("POST Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			httpresponse.setRESPONSE(response.toString());
			return httpresponse;
		} else {

			System.out.println("POST request not worked");
			httpresponse.setRESPONSE("N/A");
			return httpresponse;
		}
	}

	public HttpURLConnection getConnection(String URL, RequestType requesttype)
			throws IOException {

		URL obj = new URL(URL);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod((requesttype == RequestType.POST) ? "POST" : "GET");
		con.setRequestProperty("User-Agent", USER_AGENT);
		return con;
	}

}
